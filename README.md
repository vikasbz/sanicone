Pre-requisites:
1. Python 3.5+
2. Sanic API framework
3. Enable Gmail secure app access here: https://myaccount.google.com/lesssecureapps?pli=1


How To:
1. In a terminal, run:
"python3 main.py"

2. In a second terminal, run:
curl -H "Content-type: application/json" -X POST http://0.0.0.0:8000/api/email/send -d '{"from":"Vikas", "recipient":"vikaszingade@outlook.com", "subject":"Gotta go fast!!!", "body":"This is a Sanic API response to a POST request using JSON"}'
