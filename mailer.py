import smtplib

def send_mail(sender, password, recipient, subject, body):
    smtp_server = smtplib.SMTP_SSL("smtp.gmail.com", 465)
    smtp_server.login(sender, password)
    message = "Subject: {}\n\n{}".format(subject, body)
    smtp_server.sendmail(sender, recipient, message)
    smtp_server.close()

def make_mail(obj):
    import gmail_credentials as g

    sender, password = g.get_credentials()

    send_mail(sender, password, obj['recipient'], obj['subject'], obj['body'])

    return 0
